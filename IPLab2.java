    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    package ip.lab2;
    import java.util.Scanner;
    /**This program tracks catches of fishing expeditions for a fishing company,
     * and presents a summary after each fishing expedition.
     *
     * @author alvin.ofori
     */
    public class IPLab2 {

        /**
         * @param args the command line arguments
         * 
         */

        public static void main(String[] args) {
            // TODO code application logic here
            double[] weights;
            double weight;
            
            System.out.println("Hello. Please enter the number of catches"
                    + "you plan to make today.");
            Scanner input = new Scanner(System.in);
            int numCatches = input.nextInt();
             
            weights = new double[numCatches];
            
            for(int i=0; i<numCatches; i++){
                System.out.println("Please enter the weight for the current catch");
                weight=input.nextDouble();
                
                if (weight<0 || weight>1000){
                    System.out.println("Error: Your weight cannot be less than "
                            + "0, and cannot be greater than 1000. Please enter"
                            + "an accurate weight.");
                    weight = input.nextDouble();
            
                }
            weights[i]= input.nextDouble();
            System.out.println("The weight of the most recent catch is: "+weight);

            }
            
            
        }

    }
